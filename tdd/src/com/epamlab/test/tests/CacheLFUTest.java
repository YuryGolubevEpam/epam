package com.epamlab.test.tests;

import com.epamlab.test.beans.CacheEntry;
import com.epamlab.test.impl.LFUCache;
import org.hamcrest.MatcherAssert;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.*;

public class CacheLFUTest {

    private LFUCache<Integer, String> cacheLFU;

    @Before
    public void init() {
        cacheLFU = new LFUCache<>(7);
        cacheLFU.getCacheMap().put(1, new CacheEntry<>("first"));
        cacheLFU.getCacheMap().put(2, new CacheEntry<>("second"));
        cacheLFU.getCacheMap().put(3, new CacheEntry<>(""));
        cacheLFU.getCacheMap().put(4, new CacheEntry<>(null));
        cacheLFU.getCacheMap().put(5, new CacheEntry<>("five"));

    }

    @Test
    public void getCacheDataByKeyTest() {
        assertEquals("first", cacheLFU.get(1));
        assertEquals("second", cacheLFU.get(2));
        assertEquals("", cacheLFU.get(3));

        assertNull(cacheLFU.get(4));
        assertNotNull(cacheLFU.get(5));
    }

    @Test
    public void getAllDataTest() {
        MatcherAssert.assertThat(cacheLFU.getAll(),
                contains("first", "second", "", null, "five"));
    }

    @Test
    public void putInCacheTest() {
        assertEquals(5, cacheLFU.getCacheMap().size());
        cacheLFU.put(6, "six");
        assertEquals(6, cacheLFU.getCacheMap().size());
        cacheLFU.put(7, "seven");
        assertEquals(7, cacheLFU.getCacheMap().size());
        cacheLFU.put(8, "eight");
        assertEquals(7, cacheLFU.getCacheMap().size());
        cacheLFU.put(9, "nine");
        assertEquals(7, cacheLFU.getCacheMap().size());
    }

    @Test
    public void deleteLFUTest() {
        cacheLFU.get(1);
        cacheLFU.get(2);

        cacheLFU.get(4);
        cacheLFU.get(5);
        cacheLFU.put(6, "six");
        cacheLFU.put(7, "seven");
        cacheLFU.get(6);
        cacheLFU.get(7);

        cacheLFU.put(999, "last");

        assertEquals("last", cacheLFU.get(999));
        assertNull(cacheLFU.get(3));


    }

    @Test
    public void remoteByKeyTest() {
        assertTrue(cacheLFU.remove(1));
        assertFalse(cacheLFU.remove(9));
        assertFalse(cacheLFU.remove(null));
        assertTrue(cacheLFU.remove(2));
        assertTrue(cacheLFU.remove(3));
        assertTrue(cacheLFU.remove(4));
        assertTrue(cacheLFU.remove(5));
        MatcherAssert.assertThat(cacheLFU.getAll(), Matchers.empty());

    }
}
