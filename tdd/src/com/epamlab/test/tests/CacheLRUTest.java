package com.epamlab.test.tests;

import com.epamlab.test.impl.LRUCache;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class CacheLRUTest {
    private LRUCache<Integer, Integer> cacheLRU;

    @Before
    public void init() {
        cacheLRU = new LRUCache<>(2);
    }

    @Test
    public void testCacheStartsEmpty() {
        assertNull(cacheLRU.get(2));
    }

    @Test
    public void testSetBelowCapacity() {
        cacheLRU.put(1, 1);
        assertEquals(cacheLRU.get(1), 1);
        assertNull(cacheLRU.get(2));
        cacheLRU.put(2, 2);
        assertEquals(cacheLRU.get(1), 1);
        assertEquals(cacheLRU.get(2), 2);
    }

    @Test
    public void testCapacityReachedOldestRemoved() {
        cacheLRU.put(1, 1);
        cacheLRU.put(2, 2);
        cacheLRU.put(3, 3);
        assertNull(cacheLRU.get(1));
        assertEquals(cacheLRU.get(2), 2);
        assertEquals(cacheLRU.get(3), 3);
    }

    @Test
    public void testGetRenewsEntry() {
        cacheLRU.put(1, 1);
        cacheLRU.put(2, 4);
        assertEquals(cacheLRU.get(1), 1);
        cacheLRU.put(3, 9);
        assertEquals(cacheLRU.get(1), 1);
        assertNull(cacheLRU.get(2));
        assertEquals(cacheLRU.get(3), 9);
    }

    @Test
    public void testRemove() {
        cacheLRU.put(1, 1);
        cacheLRU.put(2, 4);
        assertEquals(2, cacheLRU.getCacheMap().size());
        cacheLRU.remove(1);
        assertEquals(1, cacheLRU.getCacheMap().size());
        cacheLRU.remove(2);
        assertEquals(0, cacheLRU.getCacheMap().size());
    }

    @Test
    public void testGetAll() {
        cacheLRU.put(1, 1);
        cacheLRU.put(2, 4);
        MatcherAssert.assertThat(cacheLRU.getAll(),
                contains(1, 4));
    }
}
