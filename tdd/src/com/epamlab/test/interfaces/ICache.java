package com.epamlab.test.interfaces;

import java.util.Collection;

public interface ICache<K,E> {
    E get(K key);
    Collection<E> getAll();
    void put(K key, E data);
    boolean remove(K key);
}
