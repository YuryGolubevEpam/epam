package com.epamlab.test.beans;

public class CacheEntry<E> {
   private E data;
   private long frequency;

    public CacheEntry(E data) {
        this.data = data;
        this.frequency = 0;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }
}
