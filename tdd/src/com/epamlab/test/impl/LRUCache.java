package com.epamlab.test.impl;

import com.epamlab.test.interfaces.ICache;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class LRUCache<K, E> implements ICache<K, E> {

    private LinkedHashMap<K, E> cacheMap = new LinkedHashMap<>();
    private final int initialCapacity;

    public LRUCache(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    @Override
    public E get(K key) {
        E value = cacheMap.get(key);
        if (value == null) {
            return null;
        } else {
           put(key, value);
        }
        return value;

    }
    @Override
    public void put(K key, E value) {
        if (cacheMap.containsKey(key)) {
            cacheMap.remove(key);
        } else if (cacheMap.size() == initialCapacity) {
            Iterator<K> it = cacheMap.keySet().iterator();
            it.next();
            it.remove();
        }
        cacheMap.put(key, value);
    }


    @Override
    public Collection<E> getAll() {
        return cacheMap.values();
    }

    @Override
    public boolean remove(K key) {
        if (cacheMap.containsKey(key)) {
            cacheMap.remove(key);
            return true;
        }
        return false;
    }

    public LinkedHashMap<K, E> getCacheMap() {
        return cacheMap;
    }
}
