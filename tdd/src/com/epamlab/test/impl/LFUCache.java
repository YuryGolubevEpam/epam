package com.epamlab.test.impl;

import com.epamlab.test.beans.CacheEntry;
import com.epamlab.test.interfaces.ICache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class LFUCache<K, E> implements ICache<K, E> {

    private LinkedHashMap<K, CacheEntry<E>> cacheMap = new LinkedHashMap<>();
    private final int initialCapacity;

    public LFUCache(int initialCapacity) {
        this.initialCapacity = initialCapacity;
    }

    @Override
    public E get(K key) {
        if (cacheMap.containsKey(key)) {
            CacheEntry<E> cacheEntry = cacheMap.get(key);
            cacheEntry.setFrequency(cacheEntry.getFrequency() + 1);
            return cacheEntry.getData();
        }
        return null;
    }

    @Override
    public Collection<E> getAll() {
        Collection<E> collection = new ArrayList<>();
        for (CacheEntry<E> entry : cacheMap.values()) {
            collection.add(entry.getData());
        }
        return collection;
    }

    @Override
    public void put(K key, E data) {
        if (isFull()) {
            K entryKeyToBeRemoved = getLFUKey();
            cacheMap.remove(entryKeyToBeRemoved);
        }
        cacheMap.put(key, new CacheEntry<>(data));
    }

    private K getLFUKey() {
        K key = null;
        long minFreq = Integer.MAX_VALUE;

        for (Map.Entry<K, CacheEntry<E>> entry : cacheMap.entrySet()) {
            if (minFreq > entry.getValue().getFrequency()) {
                key = entry.getKey();
                minFreq = entry.getValue().getFrequency();
            }
        }
        return key;
    }

    @Override
    public boolean remove(K key) {
        if (cacheMap.containsKey(key)) {
            cacheMap.remove(key);
            return true;
        }
        return false;
    }

    public LinkedHashMap<K, CacheEntry<E>> getCacheMap() {
        return cacheMap;
    }

    private boolean isFull() {
        return cacheMap.size() == initialCapacity;
    }
}
